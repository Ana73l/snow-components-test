import React, { useState, useEffect } from 'react';
import {NowApp, Router, Route, Btn, httpRequest} from './lib/index';
import NotificationContext from './contexts/NotificationContext';
import { Container, Row, Col } from 'react-bootstrap';

import Notifications from './components/Notifications';
import NotificationsCount from './components/NotificationsCount';
import Navbar from './components/Navbar/Navbar';

import Home from './pages/Home';
import Table from './pages/Table';
import Portals from './pages/Portals';

import 'bootstrap-css-only/css/bootstrap.min.css';

const sideNavLinks = [
	{
		label: 'Home',
		to: '?'
	},
	{
		label: 'Portals',
		to: '?id=portals'
	},
	{
		label: 'Tables',
		items: [
			{
				label: 'Incidents',
				to: '?id=list&table=incident'
			},
			{
				label: 'Script Includes',
				roles: ['admin'],
				to: '?id=list&table=sys_script_include'
			}
		]
	}
];

const toSeconds = n => n * 1000;
const NOTIFICATIONS_REFRESH_INTERVAL = 10;

const App = () => {
	const [notifications, setNotifications] = useState([]);	

	const userId = (window.glideUser && window.glideUser.sys_id) || null;

	const loadNotifications = () => httpRequest
		.get(`/api/now/table/x_472589_snow_comp_user_notifications?user=${userId}`)
		.then(body => body.data.result)
		.then(setNotifications)
		.catch(console.log);

	useEffect(() => {
		loadNotifications();
		const interval = setInterval(loadNotifications, toSeconds(NOTIFICATIONS_REFRESH_INTERVAL));

		return () => clearInterval(interval);
	}, []);

	return (
		<NotificationContext.Provider value={notifications}>
			<Router basename='x_472589_snow_comp_react_components_test.do' >
				<Navbar variant='dark' bg='primary' />lllool
				<Container fluid>
				<Row>
					<Col lg='12'>
						<Btn roles={['admin']}>I am admin button!</Btn>
						<Btn>I am public button!</Btn>
						<div>
							<NotificationsCount variant='primary' />
							<Notifications />
						</div>
						<Route path='/' component={Home} />
						<Route page='list' component={Table} />
						<Route path='?id=portals' component={Portals} />
					</Col>
				</Row>
				</Container>
			</Router>
			<Btn>Yolo</Btn>
		</NotificationContext.Provider>
	);
};

export default () => (
	<NowApp>
		<App />
	</NowApp>
);

