import React, { useState } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from '../../lib/index';

export default ({ brandLabel = null, ...props}) => {
    const [isOpen, setOpen] = useState(false);

    return (
        <Navbar {...props}>
            <Navbar.Brand>
                <Link to='?' >
                    <Nav.Link>
                        Newsfeed
                    </Nav.Link>
                </Link>
            </Navbar.Brand>
            <Navbar.Toggle onClick={() => setOpen(isOpen)} />
            <Navbar.Collapse in={isOpen}>
                <Nav>
                    <Nav.Item>
                    <Nav.Link>
                        Newsfeed
                    </Nav.Link>
                    </Nav.Item>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};