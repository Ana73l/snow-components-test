import React, {Fragment, useState} from 'react';
import {Collapse} from 'react-bootstrap';
import NotificationsContext from '../contexts/NotificationContext';

export default () => {
    const [isOpen, setOpen] = useState(false);

    return (
        <NotificationsContext.Consumer>
            {(notifications = null) => {
                if(!notifications) {
                    return null;
                }

                return (
                    <Fragment>
                        <p onClick={() => setOpen(!isOpen)}>
                            Open Notifications!
                        </p>
                        <Collapse in={isOpen}>
                            <div style={{ border: '1px solid blue'}}>
                                {notifications.map(({message}) => (
                                    <p>{message}</p>
                                ))}
                            </div>
                        </Collapse>
                    </Fragment>
                );
            }}
        </NotificationsContext.Consumer>
    );
};