import React from 'react';
import NotificationContext from '../contexts/NotificationContext';
import { Badge } from 'react-bootstrap';
import { withAuth } from '../lib/index';

const NotificationsCount = props => (
    <NotificationContext.Consumer>
        {(notifications = null) => {
            if(!notifications) {
                return null;
            }

            return <Badge {...props}>{notifications.length}</Badge>;
        }}
    </NotificationContext.Consumer>
);

export default ({ roles = [], ...props }) => withAuth(roles)(NotificationsCount, props);