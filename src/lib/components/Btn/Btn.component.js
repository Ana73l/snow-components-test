import React from 'react';
import {Button} from 'react-bootstrap';
import {withAuth} from '../index';

const Btn = props => {
    return <Button {...props} />;
};

export default ({ roles = [], ...props }) => withAuth(roles)(Btn, props);