import React, {useState, useEffect, Fragment} from 'react';
import {cloneDeep} from 'lodash';
import axios from 'axios';
import {UserContext} from '../../contexts/UserContext/UserContext';
import {withAuth} from '../index';

export default ({ children = null, ...props }) => {
    const { glideUser = {} } = window;
    const [user] = useState(cloneDeep(glideUser));

    useEffect(() => {
        axios.defaults.headers['X-userToken'] = user.token || '';
    }, []);

    return (
        <UserContext.Provider value={user}>
            {withAuth(props.roles || [])(() => <Fragment>{children}</Fragment>)}
        </UserContext.Provider>
    );
};